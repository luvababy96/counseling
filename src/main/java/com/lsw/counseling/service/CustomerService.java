package com.lsw.counseling.service;

import com.lsw.counseling.entity.Customer;
import com.lsw.counseling.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service // service 해주는거니까
@RequiredArgsConstructor // 협력하여 사용해야 하는 거니까
public class CustomerService { //파일을 만들며 생성되는 큰 class 는 모두가 알아야 하므로 public
    private final CustomerRepository customerRepository; // 인포에게 제공되면 안되므로 private

    public void setCustomer(String name, String phone){
        Customer addData = new Customer();
        addData.setName(name);
        addData.setPhone(phone);

        customerRepository.save(addData);
    }

}
