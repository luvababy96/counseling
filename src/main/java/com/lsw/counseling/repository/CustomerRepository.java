package com.lsw.counseling.repository;

import com.lsw.counseling.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> { //extend : 확장하다 , Jap저장소를 이용하여<어느 테이블(창고)를 다녀올건지,테이블 내 ID가 가진 자료형이 무엇인지>
}
