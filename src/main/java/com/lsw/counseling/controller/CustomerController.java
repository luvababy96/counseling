package com.lsw.counseling.controller;

import com.lsw.counseling.model.CustomerRequest;
import com.lsw.counseling.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor //혼자 쓰일 수 없음을 의미
@RequestMapping("/v1/customer") //간판을 지정해줘야하므로 mapping 사용
public class CustomerController {
    private final CustomerService customerService; //서비스 파트를 인포에서 보일 필요 없으므로 private, 서비스로 보내야 함

    @PostMapping("/data")
    public String setCustomer(@RequestBody CustomerRequest request){ //고객의 정보를 받는 곳은 노출되어야 하니 public 단, 정보를 얻고 행동이 가미되어야 하므로 body를 사용(이때 model 생성)
        customerService.setCustomer(request.getName(), request.getPhone()); //서비스에 고객에게 받은 정보를 전달해줄 것을 의미 (request.getphone = request안에 받은 phone)
        return "OK"; //고객에게 받은 정보에 따른 반응을 보여야 하므로 작성

    }

}
