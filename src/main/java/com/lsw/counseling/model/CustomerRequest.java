package com.lsw.counseling.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerRequest {

    private String name;

    private String phone;
}
