package com.lsw.counseling.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity //JPA에 있는 테이블모양
@Getter
@Setter // 주고 받는 작업이 있으니 G,S 사용
public class Customer {
    @Id // 시퀀스 생성(임의로 주어지는 번호 생성)
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 번호 생성 방식 (identity = n + 1 )
    private Long id; // 생성될 수 있는 숫자의 양을 지정(Long = 8바이트)

    @Column(nullable = false, length = 20) // column: 엑셀의 셀 한 칸을 의미 , nullable(비어있다):비어있으면 안되므로 그럴 땐 false를 띄운다고 지정, length:글자의 길이
    private String name;

    @Column(nullable = false, length = 20)
    private String Phone;

}
